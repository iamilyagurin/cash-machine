from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^card-auth$', views.CardAuthView.as_view(), name='card-auth'),
    url(r'^operations$', views.OperationsView.as_view(), name='operations'),
    url(r'^balance$', views.BalanceView.as_view(), name='balance'),
    url(r'^withdraw$', views.WithdrawView.as_view(), name='withdraw'),
    url(r'^exit', views.ExitView.as_view(), name='exit'),
]
