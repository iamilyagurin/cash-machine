# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-14 19:32
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Operation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('amount', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Сумма')),
                ('balance', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Остаток')),
                ('trans_type', models.PositiveIntegerField(choices=[(1, 'Вывод'), (2, 'Просмотр баланса')])),
                ('card', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.CreditCard')),
            ],
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='card',
        ),
        migrations.DeleteModel(
            name='Transaction',
        ),
    ]
