from django import forms

from .models import CreditCard


class CardNumberForm(forms.Form):
    number = forms.CharField(
        label='Номер карты', max_length=19, widget=forms.TextInput(attrs={'class': 'form-control'})
    )

    def clean_number(self):
        data = self.cleaned_data['number'].strip('-')

        try:
            card_number = int(data.replace('-', ''))
        except ValueError:
            raise forms.ValidationError("Номер карты введен не корректно")

        try:
            credit_card = CreditCard.objects.get(number=card_number)
        except CreditCard.DoesNotExist:
            raise forms.ValidationError("Карта не найдена")

        if credit_card.blocked:
            raise forms.ValidationError("Карта заблокирована")

        return card_number


class CardAuthForm(forms.Form):
    card_number = forms.IntegerField(widget=forms.HiddenInput)
    pin_code = forms.CharField(label='Пин код', max_length=4,
                               widget=forms.PasswordInput(attrs={'class': 'form-control'}))


class WithdrawForm(forms.Form):
    amount = forms.DecimalField(
        min_value=1,
        max_digits=10,
        decimal_places=2,
        label='Сумма',
        widget=forms.NumberInput(attrs={'class': 'form-control'})
    )
