from django.db import models
from django.conf import settings


class CreditCard(models.Model):
    number = models.BigIntegerField(verbose_name='Номер')
    pin_code = models.PositiveSmallIntegerField(verbose_name='Пин код')
    balance = models.DecimalField(max_digits=10, decimal_places=2, default=0, verbose_name='Баланс')
    blocked = models.BooleanField(default=False)
    invalid_pin_counter = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return '{}'.format(self.number)

    def invalid_pin(self):
        self.invalid_pin_counter += 1
        if self.invalid_pin_counter >= settings.PIN_ATTEMPTS:
            self.blocked = True
        self.save()

    def reset_pin_counter(self):
        if self.invalid_pin_counter > 0:
            self.invalid_pin_counter = 0
            self.save()


class Operation(models.Model):
    WITHDRAW = 1
    BALANCE = 2
    TYPES = (
        (WITHDRAW, 'Вывод'),
        (BALANCE, 'Просмотр баланса'),
    )

    card = models.ForeignKey(CreditCard)
    timestamp = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True, verbose_name='Сумма')
    balance = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True, verbose_name='Остаток')
    trans_type = models.PositiveIntegerField(choices=TYPES)

    def __str__(self):
        return '{0} | {1}'.format(self.card, self.get_trans_type_display())
