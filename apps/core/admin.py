from django.contrib import admin
from .models import CreditCard, Operation

admin.site.register(CreditCard)
admin.site.register(Operation)
