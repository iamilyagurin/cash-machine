from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.shortcuts import render, redirect
from django.views import generic

from .forms import CardNumberForm, CardAuthForm, WithdrawForm
from .models import CreditCard, Operation


class IndexView(generic.View):
    def get(self, request, *args, **kwargs):
        form = CardNumberForm()
        return render(request, 'card_number.html', {'form': form})

    def post(self, request, *args, **kwargs):
        form = CardNumberForm(request.POST)

        if form.is_valid():
            card_number = form.cleaned_data['number']
            pin_code_form = CardAuthForm(initial={'card_number': card_number})
            return render(request, 'pin_code.html', {'form': pin_code_form})

        return render(request, 'card_number.html', {'form': form})


class CardAuthView(generic.View):
    form_class = CardAuthForm
    template_name = 'pin_code.html'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            return self.form_valid(form=form)
        else:
            return redirect('index')

    def get(self, request, *args, **kwargs):
        return redirect('index')

    def form_valid(self, form):
        request = self.request
        card_number = form.cleaned_data['card_number']
        pin_code = form.cleaned_data['pin_code']

        try:
            credit_card = CreditCard.objects.get(number=card_number)
        except CreditCard.DoesNotExist:
            return redirect('index')

        if pin_code == str(credit_card.pin_code):
            request.session['current_card'] = credit_card.number
            credit_card.reset_pin_counter()
            return redirect('operations')
        else:
            credit_card.invalid_pin()
            if credit_card.blocked:
                return render(request, 'card_blocked.html', {})

            credit_card.save()
            messages.error(request, 'Неверный PIN')
            return render(request, 'pin_code.html', {'form': form})


class OperationsView(UserPassesTestMixin, generic.View):
    raise_exception = True

    def test_func(self):
        current_card = self.request.session.get('current_card')
        return current_card is not None

    def get(self, request, *args, **kwargs):
        return render(request, 'card_operations.html', {})


class BalanceView(UserPassesTestMixin, generic.View):
    raise_exception = True

    def test_func(self):
        current_card = self.request.session.get('current_card')
        return current_card is not None

    def get(self, request, *args, **kwargs):
        current_card = self.request.session.get('current_card')
        obj = CreditCard.objects.get(number=int(current_card))
        Operation.objects.create(card=obj, trans_type=Operation.BALANCE)
        return render(request, 'balance.html', {'balance': obj.balance})


class WithdrawView(UserPassesTestMixin, generic.View):
    form_class = WithdrawForm
    template_name = 'withdraw.html'
    raise_exception = True

    def test_func(self):
        current_card = self.request.session.get('current_card')
        return current_card is not None

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            return self.form_valid(form)
        return render(request, self.template_name, {'form': form})

    def form_valid(self, form):
        amount = form.cleaned_data['amount']
        current_card = self.request.session.get('current_card')

        card_obj = CreditCard.objects.get(number=int(current_card))
        if card_obj.balance < amount:
            messages.error(self.request, 'Недостаточно средст')
            return render(self.request, self.template_name, {'form': form})

        card_obj.balance -= amount
        card_obj.save()

        operation_obj = Operation.objects.create(
            card=card_obj, amount=amount, balance=card_obj.balance, trans_type=Operation.WITHDRAW
        )
        return render(self.request, 'report.html', {'operation': operation_obj})


class ExitView(generic.View):
    def get(self, request, *args, **kwargs):
        request.session.pop('current_card', None)
        return redirect('index')
