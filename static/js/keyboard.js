document.onkeydown = function (e) {
        return false;
};

$(document).ready(function () {

    var $keyboard = $('#keyboard');
    var $input = $('#' + $keyboard.attr('data-input'));
    var maxlength = parseInt($input.attr('maxlength'));

    $('.key-btn').click(function () {
        if (!(maxlength) || $input.val().length < maxlength) {
            var btnVal = $(this).html();
            $input.val($input.val() + btnVal);
            $input.trigger('change')
        }
    });

    $('.clean-btn').click(function () {
        $input.val('')
    });

});