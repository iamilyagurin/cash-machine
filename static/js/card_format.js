$(document).ready(function () {
    var $numberInput = $('#id_number');
    var maxLength = parseInt($numberInput.attr('maxlength'));
    $numberInput.on('keypress change', function () {
        $(this).val(function (index, value) {
            if (value.length < maxLength - 1){
                return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1-');
            }else{
                return value
            }

        });
    });
});